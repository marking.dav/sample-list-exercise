import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { map } from 'rxjs/operators';
import { Md5 } from 'md5-typescript';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  date = new Date();
  timestamp = this.date.getTime();
  public_key = 'bf2fe60181b304b2589e0fc615109226';
  private_key = 'fa4b7e978610291fd2fda8c6b34314953f65b461';
  hash = Md5.init(this.timestamp + this.private_key + this.public_key);

  constructor(private http: Http) {}

  getItems() {
    const headers = new Headers();
    headers.append(
      'secret-key',
      '$2a$10$H7XQ565nX1cNYliJg.ifjuhKqpJtk7mWpbL4ktHrFRbO86xuSNfV6'
    );
    return this.http
      .get('https://api.jsonbin.io/b/5b76a87cf5ef92564b09c652', {
        headers: headers
      })
      .pipe(map(res => res.json()));
  }
}
