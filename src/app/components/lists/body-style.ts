export class BodyStyle {
  private styleTag: HTMLStyleElement;
  constructor() {
    this.styleTag = this.buildStyleElement();
  }

  private buildStyleElement(): HTMLStyleElement {
    const style = document.createElement('style');
    style.type = 'text/css';
    style.setAttribute('data-debug', 'Injected by BodyStyling service.');
    style.textContent = `
            body {
                overflow: hidden !important ;
            }
        `;

    return style;
  }

  public disable(): void {
    document.body.appendChild(this.styleTag);
  }

  public enable(): void {
    document.body.removeChild(this.styleTag);
  }
}
