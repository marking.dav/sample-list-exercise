import { Component, OnInit } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import { DataService } from '../../service/data.service';
import { BodyStyle } from './body-style';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css'],
  animations: [
    trigger('slideInOut', [
      state(
        'in',
        style({
          transform: 'translate(0%,0%)'
        })
      ),
      state(
        'out',
        style({
          transform: 'translate(100%,0%)'
        })
      ),
      transition('in => out', animate('200ms ease')),
      transition('out => in', animate('200ms ease'))
    ])
  ],
  providers: [BodyStyle]
})
export class ListsComponent implements OnInit {
  items: Array<any>;
  public state = {
    details: 'out',
    itemID: null
  };

  private windowScroll: BodyStyle;

  constructor(private dataService: DataService, windowScroll: BodyStyle) {
    this.windowScroll = windowScroll;
  }

  ngOnInit() {
    this.dataService.getItems().subscribe(items => {
      this.items = items;
    });
  }

  showDetails(id) {
    this.state.details = 'in';
    this.state.itemID = id;
    this.windowScroll.disable();
  }

  dismissDetails() {
    this.state.details = 'out';
    this.windowScroll.enable();
  }
}
